import DisplayBoards from "./components/Boards/DisplayBoards";
import DisplayAllLists from "./components/Lists/DisplayAllLists";
import Header from "./components/Header";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <>
     
      <Router>
      <Header />
        <Routes>
          <Route path="/" element={<DisplayBoards />} />
          <Route path="/boards/:boardId" element={<DisplayAllLists />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
