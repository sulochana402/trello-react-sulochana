import { Box, Container, Grid, Heading, Spinner, Alert, AlertIcon } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import CreateBoard from "./CreateBoard";
import api from '../Api';

function fetchBoards() {
  return api.get('members/me/boards')
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error fetching boards:", error);
      throw error;
    });
}

function DisplayBoards() {
  const [boards, setBoards] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getBoards = async () => {
      try {
        setLoading(true);
        setError(null);

          const boardsData = await fetchBoards();
        setBoards(boardsData);
      } catch (error) {
        setError("Error fetching boards");
        console.error(error);
      } finally {
        setLoading(false);
      }
    };

    getBoards();
  }, []);

  const handleBoardCreated = (newBoard) => {
    setBoards((prevBoards) => [...prevBoards, newBoard]);
  };

  return (
    <Container maxW="container.xl" pt="12">
      <Heading mb="10">Boards</Heading>
      {loading ? (
        <Spinner size="xl" />
      ) : error ? (
        <Alert status="error">
          <AlertIcon />
          {error}
        </Alert>
      ) : (
        <Grid templateColumns="repeat(4, 1fr)" gap="4" alignItems="flex-start">
          <CreateBoard onBoardCreated={handleBoardCreated} />
          {boards.map((board) => (
            <Link key={board.id} to={`/boards/${board.id}`}>
              <Box
                bg="teal.400"
                h="16"
                textAlign='center'
                m="1"
                size="lg"
                display="flex"
                alignItems="center"
                justifyContent="center"
                color='white'
                fontSize='xl'
                borderRadius="md"
                fontWeight="bold"
                _hover={{ bg: "teal.500", cursor: "pointer" }}
              >
                {board.name}
              </Box>
            </Link>
          ))}
        </Grid>
      )}
    </Container>
  );
}

export default DisplayBoards;
