import { useRef } from "react";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import api from '../Api';

function newBoard(name) {
  return api.post(`boards`, { name })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error('Error creating board:', error);
      throw error;
    });
}

function CreateBoard({ onBoardCreated }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = useRef(null);
  

  const handleCreateBoard = async () => {
    try {
      const name = initialRef.current.value;
      const newBoardData = await newBoard(name);
      onBoardCreated(newBoardData);
      onClose();
    } catch (error) {
      console.error("Error creating board:", error);
    } 
  };

  return (
    <>
      <Button onClick={onOpen} h='16' m="1" size="lg">Create New Board</Button>

      <Modal
        initialFocusRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create a new Board</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Board Title</FormLabel>
              <Input ref={initialRef} placeholder="Enter board title" />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleCreateBoard}>
              create
            </Button>
            <Button onClick={onClose} >Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateBoard;
