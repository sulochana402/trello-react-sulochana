import axios from 'axios';

const apiKey = "8a4740d9065fe8c56882abe40d8db2ee";
const token = "ATTA7e81032976e353ef371b41f60ba683237f716cff8c051f9c4720b9cebebf1ddd91F71D67";

const api = axios.create({
  baseURL: 'https://api.trello.com/1/',
  params: {
    key: apiKey,
    token: token
  }
});

export default api;
