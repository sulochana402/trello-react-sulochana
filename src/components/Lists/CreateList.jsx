import { useRef } from "react";
import { Box, Button, Flex, FormControl, FormLabel, IconButton, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure } from "@chakra-ui/react";
import api from '../Api';
import { AddIcon } from "@chakra-ui/icons";

function createList(boardId, listName) {
  return api.post('lists', {
    name: listName,
    idBoard: boardId
  })
  .then(response => {
   return response.data;
  })
  .catch(error => {
    console.error('Error creating list:', error);
    throw error;
  });
}

function CreateList({ boardId, onListCreated }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = useRef(null);

  const handleCreateList = async () => {
    try {
      const listName = initialRef.current.value;
      const newList = await createList(boardId, listName);
      onListCreated(newList);
      onClose();
    } catch (error) {
      console.error('Error creating list:', error);
    }
  };

  return (
    <>
   <Flex onClick={onOpen} h="12" m="1" size="lg" w="250px" alignItems='center' bg='teal.200' borderRadius={5} cursor='pointer'>
     <IconButton icon={<AddIcon/>} _hover={0} bg='none' color='white' mt={0}/>
     <Box  color='white' fontSize='xl'>
        Add another list
      </Box>
    </Flex>

      <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create a new List</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>List Title</FormLabel>
              <Input ref={initialRef} placeholder="Enter list title" />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleCreateList}>
              Create
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateList;
