import { Box, Container, Flex, Heading, Spinner, Text } from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import api from '../Api'
import CreateList from "./CreateList";
import DeleteList from "./DeleteList";
import DisplayAllCards from "../Cards/DisplayAllCards";

function fetchBoardDetails(boardId) {
  return api.get(`boards/${boardId}`)
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error fetching board details:", error);
      throw error;
    });
}

function fetchLists(boardId) {
  return api.get(`boards/${boardId}/lists`)
    .then((response) =>{
      return  response.data;
    })
    .catch((error) => {
      console.error("Error fetching lists:", error);
      throw error;
    });
}

function DisplayAllLists() {
  const [lists, setLists] = useState([]);
  const [board, setBoard] = useState(null); 
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { boardId } = useParams();

  useEffect(() => {
    setLoading(true);
    setError(null);

    Promise.all([fetchBoardDetails(boardId), fetchLists(boardId)])
      .then(([boardDetails, listsData]) => {
        setBoard(boardDetails);
        setLists(listsData);
      })
      .catch((error) => {
        console.error("Error fetching board and lists:", error);
        setError("Error fetching data.");
      })
      .finally(() => setLoading(false));
  }, [boardId]);

  const handleListCreated = (newList) => {
    setLists((prevLists) => [...prevLists, newList]);
  };

  const handleDelete = (deletedListId) => {
    setLists((prevLists) => prevLists.filter((list) => list.id !== deletedListId));
  };

  return (
    <Container maxW="container.2xl" pt={12} bg="teal.300" minH='90.5vh'>
      <Heading mb="10">{loading ? 'Loading...' : (error ? 'Error Fetching Data' : (board ? board.name : ''))}</Heading>
      {error && <Text color="red.500">{error}</Text>}
      {!loading && !error && (
        <Flex gap="4" alignItems="flex-start" wrap="wrap">
          {lists.map((list) => (
            <Box
              key={list.id}
              bg="black"
              maxH="auto"
              minW='250px'
              textAlign='center'
              m={1}
              p={4}
              color='white'
              fontSize='xl'
              borderRadius="md"
              fontWeight="bold"
              cursor='pointer'
            >
              <Box display='flex' justifyContent='space-between' alignItems='center'>
                <Box>{list.name}</Box>
                <DeleteList listId={list.id} onDelete={handleDelete} />
              </Box>
              <DisplayAllCards listId={list.id} />
            </Box>
          ))}
          <CreateList boardId={boardId} onListCreated={handleListCreated} />
        </Flex>
      )}
      {loading && <Spinner />}
    </Container>
  );
}

export default DisplayAllLists;
