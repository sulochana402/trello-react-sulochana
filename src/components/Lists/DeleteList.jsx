import { CloseIcon } from "@chakra-ui/icons";
import api from "../Api";
import { IconButton } from "@chakra-ui/react";

function deleteList(listId) {
  return api.put(`lists/${listId}/closed?value=true`)
    .then(response => {
      console.log('Delete response:', response);
      return response.data;
    })
    .catch(error => {
      console.error('Error deleting list:', error);
      throw error;
    });
}

function DeleteList({ listId, onDelete }) {
  
  const handleDelete = async () => {
    try {
       await deleteList(listId);
      onDelete(listId); 
    } catch (error) {
      console.error("Error deleting list:", error);
    }
  };

  return (
    <IconButton
      bg='black'
      cursor="pointer"
      icon={<CloseIcon boxSize={4} color='white'/>}
      onClick={handleDelete}
      _hover={0} 
    />
  );
}

export default DeleteList;
