import {  IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import api from "../Api";

function deleteChecklist(checklistId) {
  return api.delete(`checklists/${checklistId}`)
    .then(response => {
      console.log("Deleted checklist:", response.data);
      return response.data;
    })
    .catch(error => {
      console.error("Error deleting checklist:", error);
      throw error;
    });
}

function DeleteChecklist({ checklistId, onDeleteChecklist }) {
  const handleDelete = async () => {
    try {
      await deleteChecklist(checklistId);
      onDeleteChecklist(checklistId);
    } catch (error) {
      console.error("Error deleting checklist:", error);
    }
  };

  return (
    <IconButton 
      icon={<CloseIcon />} 
      onClick={handleDelete} 
      aria-label="Delete Checklist"
      size="sm"
      variant="ghost"
      colorScheme="red"
    />
  );
}

export default DeleteChecklist;
