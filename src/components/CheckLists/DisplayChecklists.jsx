import { useEffect } from "react";
import { Box, Flex } from "@chakra-ui/react";
import api from "../Api";
import DeleteChecklist from "./DeleteChecklist";
import DisplayAllCheckItems from "../CheckItems/DisplayAllCheckItems";

function displayCheckLists(cardId) {
  return api
    .get(`cards/${cardId}/checklists`)
    .then((response) => {
      console.log("Display all checklists in a card:", response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("Error fetching checklists:", error);
      throw error;
    });
}

function DisplayChecklists({ cardId, checkLists, setCheckLists }) {
  useEffect(() => {
    const handleCheckLists = async () => {
      try {
        const checklistsData = await displayCheckLists(cardId);
        setCheckLists(checklistsData);
      } catch (error) {
        console.error("Error fetching checklists:", error);
      }
    };
    handleCheckLists();
  }, [cardId]);

  const handleDeleteChecklist = (checklistId) => {
    setCheckLists((prevCheckLists) =>
      prevCheckLists.filter((checklist) => checklist.id !== checklistId)
    );
  };



  return (
    <>
      <Flex flexWrap="wrap" display="flex" flexDirection="column">
        {checkLists.map((checkList) => (
          <Box
            key={checkList.id}
            bg="white"
            maxH="auto"
            minW="300px"
            display="flex"
            flexDirection="column"
            m={1}
            p={3}
            color="black"
            fontSize="md"
            borderRadius="md"
          >
            <Flex justifyContent="space-between" alignItems="center">
              <Box fontWeight="bold">{checkList.name}</Box>
              <DeleteChecklist
                checklistId={checkList.id}
                onDeleteChecklist={handleDeleteChecklist}
              />
            </Flex>
            <DisplayAllCheckItems checklistId={checkList.id} cardId={cardId} />
          </Box>
        ))}
      </Flex>
    </>
  );
}

export default DisplayChecklists;
