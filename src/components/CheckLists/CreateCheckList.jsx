import { useRef, useState } from "react";
import api from "../Api";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";

function createNewChecklist(cardId, checkListName) {
  return api
    .post(`cards/${cardId}/checklists`, {
      name: checkListName,
    })
    .then((response) => {
      console.log("created checklist:", response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("Error creating checklist", error);
      throw error;
    });
}

function CreateCheckList({ cardId, setCheckLists }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = useRef(null);
  const [checklistName, setChecklistName] = useState("");

  const newChecklist = async () => {
    try {
      const newChecklist = await createNewChecklist(cardId, checklistName);
      handleChecklistCreated(newChecklist);
      setChecklistName("");
      onClose();
    } catch (error) {
      console.error(error);
    }
  };

  const handleChecklistCreated = (newList) => {
    // console.log(newList);
    const updatedChecklist = (prevlists) => {
      return [...prevlists, newList];
    };
    setCheckLists(updatedChecklist);
  };

  return (
    <>
      <Flex m="1" alignSelf="flex-end" flexDirection="column" mr={0} mb={3}>
        <Heading fontSize="md" mb={3} mt={0}>
          Add to card
        </Heading>
        <Box
          color="white"
          fontSize="xl"
          size="lg"
          w="120px"
          h="12"
          onClick={onOpen}
          borderRadius={5}
          bg="teal.200"
          justifyContent="center"
          textAlign="center"
          alignContent="center"
          cursor="pointer"
        >
          CheckList
        </Box>
      </Flex>

      <Modal
        initialFocusRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add new checklist</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>CheckList Title</FormLabel>
              <Input
                ref={initialRef}
                placeholder="Enter checklist title"
                value={checklistName}
                onChange={(e) => setChecklistName(e.target.value)}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={newChecklist}>
              Add
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateCheckList;
