import { useState } from "react";
import {  Button, FormControl, FormLabel, Input, useDisclosure, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/react";
import api from "../Api";

function createCheckItem(checklistId, itemName) {
  return api
    .post(`checklists/${checklistId}/checkitems`, { name: itemName })
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error creating check item", error);
      throw error;
    });
}

function CreateCheckItem({ checklistId, onCheckItemCreated }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [itemName, setItemName] = useState("");

  const handleCreateCheckItem = async () => {
    try {
      const newCheckItem = await createCheckItem(checklistId, itemName);
      onCheckItemCreated(newCheckItem);
      setItemName("");
      onClose();
    } catch (error) {
      console.error("Error creating check item", error);
    }
  };

  return (
    <>
      <Button onClick={onOpen} width='100px'>Add an Item</Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create New Check Item</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Item Name</FormLabel>
              <Input value={itemName} onChange={(e) => setItemName(e.target.value)} />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateCheckItem} mr={2} bg='blue.200'>Create</Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateCheckItem;
