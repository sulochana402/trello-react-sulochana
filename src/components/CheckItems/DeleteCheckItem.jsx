import { IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import api from "../Api";

function deleteCheckItem(checklistId, checkItemId) {
  return api
    .delete(`checklists/${checklistId}/checkitems/${checkItemId}`)
    .then((response) => {
      console.log("Deleted check item:", response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("Error deleting check item:", error);
      throw error;
    });
}

function DeleteCheckItem({ checklistId, checkItemId, onDelete }) {
  const handleDelete = async () => {
    try {
      await deleteCheckItem(checklistId, checkItemId);
      onDelete(checkItemId);
    } catch (error) {
      console.error("Error deleting check item:", error);
    }
  };

  return (
    <IconButton
    icon={<CloseIcon boxSize={3} color='black'/>}
    aria-label="Delete check item"
    onClick={handleDelete}
    bg='none'
    />
  );
}

export default DeleteCheckItem;
