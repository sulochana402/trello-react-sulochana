import { useEffect, useState } from "react";
import { Flex, Text, Progress, Checkbox } from "@chakra-ui/react";
import api from "../Api";
import DeleteCheckItem from "./DeleteCheckItem";
import CreateCheckItem from "./CreateCheckItem";

function fetchCheckItems(checklistId) {
  return api
    .get(`checklists/${checklistId}/checkitems`)
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error fetching check items:", error);
      throw error;
    });
}

function updateCheckItemStatus(cardId, checkItemId, isChecked, newState) {
  return api.put(`cards/${cardId}/checkitem/${checkItemId}`, {
    checked: isChecked,
    state: newState,
  });
}

function DisplayAllCheckItems({ checklistId, cardId }) {
  const [checkItems, setCheckItems] = useState([]);

  useEffect(() => {
    const handleCheckItems = async () => {
      try {
        const checkItemsData = await fetchCheckItems(checklistId);
        setCheckItems(checkItemsData);
      } catch (error) {
        console.error("Error fetching check items:", error);
      }
    };
    handleCheckItems();
  }, [checklistId]);

  const handleDeleteCheckItem = (checkItemId) => {
    setCheckItems((prevCheckItems) =>
      prevCheckItems.filter((item) => item.id !== checkItemId)
    );
  };

  const handleToggleCheck = async (checkItemId, isChecked) => {
    try {
      const newState = isChecked ? "incomplete" : "complete";
      await updateCheckItemStatus(cardId, checkItemId, isChecked, newState);

      setCheckItems((prevCheckItems) =>
        prevCheckItems.map((item) =>
          item.id === checkItemId ? { ...item, state: newState } : item
        )
      );
    } catch (error) {
      console.error("Error updating check item status:", error);
    }
  };

  const handleCreateCheckItem = (newCheckItem) => {
    setCheckItems((prevCheckItems) => [...prevCheckItems, newCheckItem]);
  };

  const completedItems = checkItems.filter((item) => item.state === "complete").length;
  const totalItems = checkItems.length;
  const progress = totalItems ? (completedItems / totalItems) * 100 : 0;

  return (
    <>
      <Progress
        value={progress}
        size="sm"
        mb={2}
        borderRadius={4}
        colorScheme={progress === 100 ? "green" : "blue"}
      />
      {checkItems.map((item) => (
        <Flex key={item.id} alignItems="center" justifyContent="space-between">
          <Flex>
            <Checkbox
              isChecked={item.state === "complete"}
              onChange={() => handleToggleCheck(item.id, item.state === "complete")}
              colorScheme="green"
            />
            <Text ml={2} textDecoration={item.state === "complete" ? "line-through" : ""}>
              {item.name}
            </Text>
          </Flex>
          <DeleteCheckItem
            checklistId={checklistId}
            checkItemId={item.id}
            onDelete={() => handleDeleteCheckItem(item.id)}
          />
        </Flex>
      ))}
      <CreateCheckItem
        checklistId={checklistId}
        onCheckItemCreated={handleCreateCheckItem}
      />
    </>
  );
}

export default DisplayAllCheckItems;
