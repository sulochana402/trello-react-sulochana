import { Flex, Heading, Image } from "@chakra-ui/react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <Flex
      alignItems="center"
      padding="1rem"
      bg="teal.500"
      color="white"
      mx="auto"
    >
      <Link to="/boards">
        <Image src="/home.png" w={10} h={8} />
      </Link>

      <Flex flex="1" justify="center">
        <Heading>Trello</Heading>
      </Flex>
    </Flex>
  );
}

export default Header;
