import { IconButton } from "@chakra-ui/react";
import api from "../Api";
import { CloseIcon } from "@chakra-ui/icons";

function deleteCard(cardId, cardName) {
  return api
    .put(`cards/${cardId}/closed?value=true`, {
      name: cardName,
      idCard: cardId,
    })
    .then((response) => {
      console.log("Delete response:", response);
      return response.data;
    })
    .catch((error) => {
      console.error("Error Delete card:", error);
      throw error;
    });
}

function DeleteCard({ cardId, onDeleteCard }) {
  
  const handleDeleteCard = async (event) => {
    try {
      event.stopPropagation();
      await deleteCard(cardId);
      onDeleteCard(cardId);
    } catch (error) {
      console.error("Error deleting card:", error);
    }
  };

  return (
    <>
      <IconButton
        bg="gray.700"
        cursor="pointer"
        icon={<CloseIcon boxSize={3} color="white" />}
        onClick={handleDeleteCard}
        _hover={0}
      />
    </>
  );
}

export default DeleteCard;
