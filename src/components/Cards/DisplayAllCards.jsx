import { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  useDisclosure,
} from "@chakra-ui/react";
import api from "../Api";
import CreateCard from "./CreateCard";
import DeleteCard from "./DeleteCard";
import DisplayChecklists from "../CheckLists/DisplayChecklists";
import CreateCheckList from "../CheckLists/CreateCheckList";

function fetchCards(listId) {
  return api
    .get(`lists/${listId}/cards`)
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error fetching cards:", error);
      throw error;
    });
}

function DisplayAllCards({ listId }) {
  const [checkLists, setCheckLists] = useState([]);
  const [cards, setCards] = useState([]);
  const [selectedCard, setSelectedCard] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    const getCards = async () => {
      try {
        const cardsData = await fetchCards(listId);
        setCards(cardsData);
      } catch (error) {
        console.error("Error fetching cards:", error);
      }
    };
    getCards();
  }, [listId]);

  const handleCardCreated = (newCard) => {
    setCards((prevCards) => [...prevCards, newCard]);
  };

  const handleToDeleteCard = (deleteCardId) => {
    setCards((prevCards) =>
      prevCards.filter((card) => card.id !== deleteCardId)
    );
  };

  const handleCardClick = (card) => {
    setSelectedCard(card);
    onOpen();
  };

  return (
    <>
      <Flex flexWrap="wrap" mt={4} display="flex" flexDirection="column">
        {cards.map((card) => (
          <Box
            key={card.id}
            bg="gray.700"
            maxH="auto"
            minW="200px"
            m={1}
            p={3}
            color="white"
            fontSize="md"
            borderRadius="md"
            onClick={() => handleCardClick(card)}
          >
            <Box>
              <Flex justifyContent="space-between" alignItems="center">
                <Box>{card.name}</Box>
                <DeleteCard
                  cardId={card.id}
                  onDeleteCard={handleToDeleteCard}
                />
              </Flex>
            </Box>
          </Box>
        ))}
        <CreateCard listId={listId} onCardCreated={handleCardCreated} />
      </Flex>

      {selectedCard && (
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
          <ModalOverlay
            bg="blackAlpha.300"
            backdropFilter="blur(10px) hue-rotate(90deg)"
          />
          <ModalContent width="80%" minHeight='auto'>
            <ModalHeader>{selectedCard.name}</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Flex flexDirection='column' alignItems="center" mt={4} width="100%" height="100%" >
              <CreateCheckList
                  cardId={selectedCard.id}
                  checkLists={checkLists}
                  setCheckLists={setCheckLists}
                />
                <DisplayChecklists
                  cardId={selectedCard.id}
                  checkLists={checkLists}
                  setCheckLists={setCheckLists}
                />
              </Flex>
            </ModalBody>
          </ModalContent>
        </Modal>
      )}
    </>
  );
}

export default DisplayAllCards;
