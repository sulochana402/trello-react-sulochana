import { Box, Button, Flex, FormControl, FormLabel, IconButton, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure } from "@chakra-ui/react";
import api from "../Api";
import { useRef } from "react";
import { AddIcon } from "@chakra-ui/icons";

function createNewCard(listId, cardName) {
  return api.post('cards', {
    name: cardName,
    idList: listId,
  })
  .then((response) =>{
    console.log("created new card:", response.data);
   return response.data
})
  .catch((error) => {
    console.error("Error creating Card:", error);
    throw error;
  });
}

function CreateCard({ listId, onCardCreated }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = useRef(null);
  

  const getCreatedCard = async () => {
    try {
      const cardName = initialRef.current.value;

      const newCard = await createNewCard(listId, cardName);
      onCardCreated(newCard);
      onClose();
    } catch (error) {
      console.error("Error creating card:", error);
    } 
  };

  return (
    <>
     <Flex onClick={onOpen} h="12" m="1" size="lg" w="250px" alignItems='center' cursor='pointer'>
     <IconButton icon={<AddIcon/>} _hover={0} bg='none' color='white' mt={0}/>
     <Box fontSize='lg'>
        Add a card
      </Box>
     </Flex>

      <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create a new Card</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Card Title</FormLabel>
              <Input
                ref={initialRef}
                placeholder="Enter card title"
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={getCreatedCard}>
              create
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateCard;
